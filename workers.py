import multiprocessing
import threading
import select
import sqlite3
import xmltodict
import parsers
import constants as c


class SQLiteWorker(multiprocessing.Process):
    def __init__(self, tasks):
        super().__init__()

        with open('queries.xml') as f:
            xml = xmltodict.parse(f.read())
            self.queries = xml['queries']

        self.tasks = tasks

    def run(self):
        while True:
            payload = self.tasks.get()
            with sqlite3.connect('khronos.db') as db:
                db.execute(self.queries['insert'], payload)


class ReaderWorker(threading.Thread):
    def __init__(self, socket, responses):
        super().__init__()

        self.socket = socket
        self.responses = responses
        self.dispatcher = parsers.Dispatcher()
        self.complete = parsers.CompleteResponse()

    def run(self):
        inputs = [self.socket]

        while True:
            readers, _, _ = select.select(inputs, [], [])

            for reader in readers:
                payload = reader.recv(4096)
                messages = self.unpack(payload)

                for message in messages:
                    response = self.dispatcher.dispatch(message)
                    self.responses.put(response)

                self.responses.put(self.complete)

    def unpack(self, payload):
        # print(''.join('[{:02x}]'.format(r) for r in payload))
        # current buffer position
        index = 0
        # buffer length
        length = len(payload)
        messages = []

        # loop while there are buffer parts to split
        # print('orig:', ''.join('[{:02x}]'.format(m) for m in payload))
        while index < length:
            # print('index, length:', index, length)
            if length > index + 1:
                # check if index is at the message head
                if payload[index] == c.HEADER:
                    # move one byte to get message length
                    message_length = payload[index + 1]
                    # validate remaining buffer with reported message length
                    if index + message_length + 1 < length:
                        # copy the message to a new buffer
                        message = payload[index:index + message_length + 2]
                        messages.append(message)
                        index += message_length + 1
                        # pdb.set_trace()
                        # print('top:', index, ''.join('[{:02x}]'.format(m) for m in message))
                        # print(index, length)

            index += 1

        return messages

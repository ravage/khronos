import datetime


def checksum(buffer):
    start = 0
    end = len(buffer)

    sum_bytes = 0x00
    for i in range(start, end):
        sum_bytes += buffer[i]

    return ((~sum_bytes) + 1) & 0xFF


def add_seconds(timestamp, offset):
    return timestamp + datetime.timedelta(seconds=offset)


def is_above_wait_time(timestamp, offset, current):
    return timestamp is None or add_seconds(timestamp, offset) < current


def bytes_to_int(value):
    return int.from_bytes(value, 'big')

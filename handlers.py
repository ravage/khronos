import datetime
import constants as c
import threading
import commands
import utils
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s %(levelname)s %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger(__name__)


class ResponseHandler(threading.Thread):
    def __init__(self, persister):
        super().__init__()

        self.responses = None
        self.channel = None
        self.persister = persister

    @property
    def _handlers(self):
        return {
            c.CMD_REALTIME_INVENTORY: RealtimeHandler(self.persister),
            c.CMD_FAST_SWITCH_ANTENNA_INVENTORY: RealtimeHandler(self.persister),
            c.COMPLETE: CompleteHandler()
        }

    def run(self):
        while True:
            response = self.responses.get()
            self._handlers.get(response.command, UnknownHandler()).handle(self.channel, response)

            # if response.command == c.CMD_REALTIME_INVENTORY:
            #     self.channel.send(commands.RealtimeInventoryCommand())
            # elif response.command == c.CMD_FAST_SWITCH_ANTENNA_INVENTORY:
            #     print(response.is_read)
            #     if response.is_read:
            #         print('{} {}'.format(response.number, response.antenna))
            #     elif response.is_info:
            #         print('info')
            #     elif response.is_failure:
            #         print(response.antenna)
            #     self.channel.send_loop(
            #         commands.FastSwitchAntennaInventoryCommand())


class RealtimeHandler(object):
    def __init__(self, persister):
        self.timestamps = {}
        self.entries = []
        self.persister = persister

    def handle(self, channel, response):
        if response.is_read:
            logger.info('{} {}'.format(response.number, response.antenna))
            timestamp = self.timestamps.get(response.number)
            current_time = datetime.datetime.now()

            if utils.is_above_wait_time(timestamp, c.OFFSET, current_time):
                self.timestamps[response.number] = current_time
                entry = {
                    'number': response.number,
                    'timestamp': current_time,
                    'antenna': response.antenna
                }
                self.entries.append(entry)
                self.persister and self.persister.put(entry)
        elif response.is_info:
            logger.info('Ping...')


class UnknownHandler(object):
    def handle(self, channel, response):
        print(response.error)
        print(''.join('{:02x} - '.format(r) for r in response.raw))
        channel.send(commands.FastSwitchAntennaInventoryCommand())


class CompleteHandler(object):
    def handle(self, channel, response):
        channel.send(commands.FastSwitchAntennaInventoryCommand())

class ResponseParser(object):

    def __init__(self):
        self.timestamps = {}
        self.entries = []

    @property
    def _mappings(self):
        return {
            c.CMD_GET_FIRMWARE: self.firmware,
            c.CMD_GET_FREQUENCY_REGION: self.frequency_region,
            c.CMD_SET_BEEPER_MODE: self.beeper,
            c.CMD_GET_READER_TEMPERATURE: self.temperature,
            c.CMD_SET_WORK_ANTENNA: self.antenna,
            c.CMD_REALTIME_INVENTORY: self.realtime_inventory,
            c.CMD_FAST_SWITCH_ANTENNA_INVENTORY: self.fast_switch_inventory
        }

    def _mapping(self, key):
        return self._mappings.get(key, self._method_missing)

    def _method_missing(self, payload):
        print('Miss: {}'.format(payload))
        print(''.join('{:02x} - '.format(p) for p in payload))

    def __call__(self, payload):
        return self._mapping(payload[3])(payload)

    def firmware(self, payload):
        print('{}.{}'.format(payload[4], payload[5]))

    def frequency_region(payload):
        print(payload[4])

    def beeper(self, payload):
        print('Beeper: {}'.format(payload[1]))

    def temperature(self, payload):
        print('{} {}'.format(payload[4], payload[5]))

    def antenna(self, payload):
        print('Antenna {}'.format(payload))

    def realtime_inventory(self, payload):
        return RealtimeResponse(payload)
        # if len(payload) == 8:
        #     print('A: {}, T: {} '.format(
        #         int.from_bytes(payload[1:4], 'big'),
        #         int.from_bytes(payload[7:11], 'big'))
        #     )
        # else:
        #     # pc = payload[2:4]
        #     # epc = payload[4:16]
        #     # antenna = utils.bytes_to_int(payload[1:2]) & 0x03
        #     # frequency = utils.bytes_to_int(payload[1:2]) >> 2
        #     # number = binascii.hexlify(epc).decode().lstrip('0')
        #
        #     timestamp = self.timestamps.get(number)
        #     current_time = datetime.datetime.now()
        #
        #     if utils.is_above_wait_time(timestamp, c.OFFSET, current_time):
        #         pass
        #         # print('{} {} {} {}'.format(
        #         #     current_time, number, antenna, frequency))
        #         # self.timestamps[number] = current_time
        #         # entry = {
        #         #     'number': number,
        #         #     'timestamp': current_time,
        #         #     'antenna': antenna
        #         # }
        #         # self.entries.append(entry)
        #         # data_queue.put(entry)

    def fast_switch_inventory(self, payload):
        return self.realtime_inventory(payload)

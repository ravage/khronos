import constants as c
import utils


class BeeperCommand(object):
    def __init__(self, mode=c.BEEP_INVENTORY_ROUND):
        self.mode = mode

    def __call__(self):
        payload = [0xA0, 0x04, 0xFF, c.CMD_SET_BEEPER_MODE, self.mode]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class FirmwareCommand(object):
    def __call__(self):
        payload = [0xA0, 0x03, 0xFF, c.CMD_GET_FIRMWARE]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class FrequencyRegionCommand(object):
    def __call__(self):
        payload = [0xA0, 0x03, 0xFF, c.CMD_GET_FREQUENCY_REGION]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class TemperatureCommand(object):
    def __call__(self):
        payload = [0xA0, 0x03, 0xFF, c.CMD_GET_READER_TEMPERATURE]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class RealtimeInventoryCommand(object):
    def __call__(self):
        payload = [0xA0, 0x04, 0xFF, c.CMD_REALTIME_INVENTORY, 0xFF]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class SetAntennaCommand(object):
    def __init__(self, antenna=0x00):
        self.antenna = antenna

    def __call__(self):
        payload = [0xA0, 0x04, 0xFF, c.CMD_SET_WORK_ANTENNA, self.antenna]
        payload.append(utils.checksum(payload))
        return bytes(payload)


class FastSwitchAntennaInventoryCommand(object):
    def __call__(self):
        payload = [
            0xA0,
            0x0D,
            0xFF,
            c.CMD_FAST_SWITCH_ANTENNA_INVENTORY,
            0x00,
            0x01,
            0x01,
            0x01,
            0x04,
            0x01,
            0x04,
            0x01,
            0x00,
            0x0A
        ]
        payload.append(utils.checksum(payload))
        return bytes(payload)

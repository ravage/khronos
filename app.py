import client
import commands
import workers
import multiprocessing
import handlers

if __name__ == '__main__':
    writer_queue = multiprocessing.Queue()
    handler = handlers.ResponseHandler(writer_queue)
    reader = client.ReaderClient('192.168.0.178', 4001, handler)
    channel = reader.connect()
    writer_worker = workers.SQLiteWorker(writer_queue)
    writer_worker.start()
    handler.start()
    channel.send(commands.FastSwitchAntennaInventoryCommand())

import constants as c
import binascii


class Dispatcher(object):
    @property
    def _mappings(self):
        return {
            c.CMD_GET_FIRMWARE: FirmwareResponse(),
            c.CMD_GET_FREQUENCY_REGION: FrequencyRegionResponse(),
            c.CMD_SET_BEEPER_MODE: BeeperResponse(),
            c.CMD_GET_READER_TEMPERATURE: TemperatureResponse(),
            c.CMD_SET_WORK_ANTENNA: AntennaResponse(),
            c.CMD_REALTIME_INVENTORY: RealtimeResponse(),
            c.CMD_FAST_SWITCH_ANTENNA_INVENTORY: RealtimeResponse()
        }

    def dispatch(self, raw):
        response = self._mappings.get(raw[3], UnknownResponse())
        # print(''.join('[{:02x}]'.format(r) for r in raw))
        return response(raw)


class Response(object):
    def __init__(self, raw):
        self.raw = raw
        self._parse()

    def _parse(self):
        length = self.raw[1]
        self.command = self.raw[3]
        self.check = self.raw[-1]
        self.data = self.raw[4:length + 1]


class RealtimeResponse(object):
    def __call__(self, raw):
        self.is_read = False
        self.is_info = False
        self.is_failure = False
        self.is_error = False

        self.response = Response(raw)

        if len(self.response.data) == 7:
            self.is_info = True
        else:
            self.is_read = True
            self.pc = self.response.data[1:3]
            self.epc = self.response.data[3:15]
            self.antenna = self._antenna()
            self.frequency = self._frequency()
            self.number = self._number()

        return self

    def _antenna(self):
        return int.from_bytes(self.response.data[0:1], 'big') & 0x03

    def _frequency(self):
        return int.from_bytes(self.response.data[0:1], 'big') >> 2

    def _number(self):
        return binascii.hexlify(self.epc).decode().lstrip('0')

    def __getattr__(self, name):
        return getattr(self.response, name)


class ErrorResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.error = self.response.data[0]
        return self


class BeeperResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.code = self.response.data[0]
        return self


class FirmwareResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.major, self.minor = self.response.data
        return self


class FrequencyRegionResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.region = self.response.data[0]
        self.start_frequency = self.response.data[1]
        self.end_frequency = self.response.data[2]
        return self


class TemperatureResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.sign = '+' if self.response.data[0] == 0x00 else '-'
        self.temperature = self.response.data[1]
        return self


class AntennaResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.code = self.response.data[0]
        return self


class UnknownResponse(object):
    def __call__(self, raw):
        self.response = Response(raw)
        self.command = 'unknown'
        self.error = 'unknown'
        return self

    def __getattr__(self, name):
        return getattr(self.response, name)


class CompleteResponse(object):
    def __init__(self):
        self.command = c.COMPLETE

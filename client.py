# 192.168.0.178

import socket
import queue
from workers import ReaderWorker

entries = []
timestamps = {}
antennas = [0x00, 0x01, 0x02, 0x03]
idx = 1
print('Running...')


class ReaderClient(object):
    def __init__(self, ip='192.168.0.178', port=4001, handler=None):
        self.address = (ip, port)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.handler = handler

    def connect(self):
        print('Conecting...')
        self.socket.connect(self.address)
        self.channel = Channel(self.socket, self.handler)
        self.channel.listen()
        print('Connected')
        return self.channel


class Channel(object):
    def __init__(self, socket, handler):
        self.socket = socket
        self.responses = queue.Queue()
        self.reader = ReaderWorker(socket, self.responses)

        self.handler = handler
        self.handler.channel = self
        self.handler.responses = self.responses

    def listen(self):
        self.reader.start()

    def send(self, command):
        self.socket.send(command())
